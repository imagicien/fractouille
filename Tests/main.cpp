#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "ChainSubstituer.h"

//Exemple de test pour les goglus: Chaque fois que vous faites des tests pour une nouvelle classe, cr�ez vous un nouveau fichier .cpp
//appel� : NomDeLaClasseTest.cpp. Ensuite vous pouvez faire diff�rents test case comme ci-dessous.
//VOIR : https://github.com/philsquared/Catch/blob/master/docs/Readme.md pour plus d'info :)

TEST_CASE("Mon beau test", "[test1]") { // "mon beau test" est le nom du test case et "[test1]" est le tag du test, �crivez ce qu'il vous plai.
	
	SECTION("PREMIER TEST ICI"){//vous pouvez cr�er des sections pour am�liorer la visibilit�

		REQUIRE(1 == 1);//vous faites des REQUIRE (ressemble a des assert :))
	}
}