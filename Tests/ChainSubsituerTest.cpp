#include "catch.hpp"
#include "ChainSubstituer.h"

map<char, list<char>> m_rules;

list<char> listeTest1 = { 'a', 'b', 'c' };
int nivRecursion1 = 1;
int nivRecursion2 = 2;

TEST_CASE("ChainSubsituerTest : Test Constructeur", "[Constructeur]") {
	
}

TEST_CASE("ChainSubsituerTest : Test substitute", "[substitute]") {

	//initialisation d'un alphabet
	m_rules.emplace('a', list<char>{'a'});
	m_rules.emplace('b', list<char>{'a','b'});
	m_rules.emplace('c', list<char>{'b','b'});
	ChainSubstituer chain1(m_rules);

	list<char> result1 = chain1.susbtitute(nivRecursion1, listeTest1);
	list<char> attendu1 = { 'a', 'a', 'b', 'b', 'b' };
	REQUIRE(result1 == attendu1);

	list<char> result2 = chain1.susbtitute(nivRecursion2, listeTest1);
	list<char> attendu2 = { 'a', 'a', 'a','b', 'a', 'b','a','b' };
	REQUIRE(result2 == attendu2);
}
