**Membres d'équipe :** 

Jérémie Coulombe 13 061 991

Marie-Eve Dubé 13 069 289

Alexandre Blouin 13 094 634

Charles-Olivier Gosselin 13 023 517

Marc-Antoine Nadeau 13 065 274

Carl Lemaire 13 033 549

Mathieu Grondin 13 074 059

Jean-Philippe Lavoie 13 085 822

---

**Titre et description**

Générateur de fractales

Nous voulons créer un petit logiciel qui permet a l'utilisateur de générer des fractales personnalisées a partir d'une liste d'instructions et quelques paramètres ajustables.

**Version alpha**

Comme version de base nous avons implémenté un algorithme de substitution de chaînes de caractères. On demande a l'utilisateur d'entrer un alphabet et des règles de substitution ainsi qu'un degré de récursivité et le logiciel produit la chaîne résultante a partir de ces consignes.

 ---

**Ajouts prévus (a modifier)**

- établir un lien entre "alphabet" et les effets visuels souhaités. (traits,courbes,angles au lieu de a,b,c...)

- permettre l'écriture dans un fichier (.jpg , .png etc.)

- préconcevoir certaines règles qui peuvent être sélectionnées par l'utilisateur

- intégration de la couleur 

- gestion de la taille de l'image

- création d'une interface

- gestion de l'espace (subdivision, faire plusieurs fractales dans le même fichier)

- affichage de l'image

- utilisation de la souris pour "manipuler" l'image resultante

- gestion des "combo box"

- permettre l'importation d'images déja concues

- possibilité de rendu 3D

---

**Tests unitaires**

Choix possibles:

* Catch http://catch-lib.net (Carl et Marc l'ont essayé et ça marche bien)

* http://unittest-cpp.sourceforge.net/