#pragma once

#define _USE_MATH_DEFINES
#include <cmath>
#include "Tourner.h"

void Tourner::execute(DrawingState& a_ds) const
{
	double angle = deg2rad(m_parameters[0]); //convertir en radians
	
	//Ramener sur [0, 2*pi]
	angle = fmod(angle, 2*M_PI);
	if (angle < 0)
		angle += 2 * M_PI;
	
	a_ds.m_angleCourant -= angle;
}

string Tourner::getXMLName() const
{
	return "tourner";
}