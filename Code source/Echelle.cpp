#pragma once

#include "Echelle.h"

void Echelle::execute(DrawingState& a_ds) const
{
	a_ds.m_longueur *= m_parameters[0];
}

string Echelle::getXMLName() const
{
	return "echelle";
}