#pragma once

#include "Action.h"
#include "types.h"

class Fractal
{
public:
	Fractal(string a_name, bool a_isReadOnly);
	Fractal();
	~Fractal();

	void setName(string a_name);
	void setSeed(string a_seed);
	void addSubstitutionRule(char a_symbol, string a_substitutionRule);
	void removeSubstitutionRule(char a_symbol);
	void addInstruction(char a_symbol, Action* a_action);
	void removeInstruction(char a_symbol);
	void setLengthRatio(double a_lengthRatio);

	string getName();
	list<char> getSeed();
	substitutionRules_t getSubstitutionRules();
	double getLengthRatio();
	bool isReadOnly();
	map<char, Action*>& getActions();

private:
	string m_name;
	list<char> m_seed;
	substitutionRules_t m_substitutionRules;
	double m_lengthRatio;
	bool m_isReadOnly;
	map<char, Action*> m_actions;
};