#include "Point.h"

Point::Point()
:m_x(0),
m_y(0)
{}

Point::Point(double a_x, double a_y)
:m_x(a_x),
m_y(a_y)
{}

double Point::getX()
{
	return m_x;
}

double Point::getY()
{
	return m_y;
}

int Point::getIntX()
{
	return round(m_x);
}

int Point::getIntY()
{
	return round(m_y);
}