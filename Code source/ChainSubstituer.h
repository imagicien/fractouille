#pragma once
#include <iostream>
#include <list>
#include <map>
#include <string>

using namespace std;

class ChainSubstituer
{
public:
	ChainSubstituer(void);
	ChainSubstituer(map<char,list<char>>);
	void saveFile();
	~ChainSubstituer(void);
	void init();
	list<char> susbtitute(int, list<char>);
	

private:
	map<char,list<char>> m_rules;
};

