#include "MaitreDessinateur.h"

MaitreDessinateur::MaitreDessinateur(Image& a_image, int a_recursion_lvl)
{
	m_drawingState.m_position = Point(a_image.image.height() * 0.5, a_image.image.height() * 0.5);
	m_drawingState.m_image = &a_image;
	m_drawingState.m_angleCourant = 0.0;
	m_drawingState.m_nivRecursion = a_recursion_lvl;
	m_drawingState.m_couleur[0] = 255.0;
	m_drawingState.m_couleur[1] = 255.0;
	m_drawingState.m_couleur[2] = 255.0;
}

MaitreDessinateur::~MaitreDessinateur()
{}

void MaitreDessinateur::dessine(list<char> a_sequence)
{
	cout << endl << "Dessin en cours..." << endl;

	size_t i = 1;
	size_t total = a_sequence.size();

	for (auto carac : a_sequence)
	{
		m_fractal.getActions()[carac]->execute(m_drawingState);

		cout << '\r' << 100 * i / total << "%  " << flush;
		i++;
	}
}

void MaitreDessinateur::setFractal(Fractal a_fractal)
{
	m_fractal = a_fractal;

	if (m_fractal.getLengthRatio() + 1 < 1e-5)
		m_drawingState.m_longueur = m_drawingState.m_image->getHeight();
	else
		m_drawingState.m_longueur = m_drawingState.m_image->getHeight() / pow(m_fractal.getLengthRatio(), m_drawingState.m_nivRecursion);
}

void MaitreDessinateur::setPosition(int x, int y)
{
	m_drawingState.m_position = Point(x, y);
}

void MaitreDessinateur::modifierLongueur(double coeff)
{
	m_drawingState.m_longueur *= coeff;
}

void MaitreDessinateur::setCouleur(Couleur a_couleur)
{
	m_drawingState.m_couleur[0] = a_couleur[0];
	m_drawingState.m_couleur[1] = a_couleur[1];
	m_drawingState.m_couleur[2] = a_couleur[2];
}