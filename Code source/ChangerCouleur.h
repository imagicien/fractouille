#pragma once

#include "Action.h"



class ChangerCouleur : public Action
{
public :
	void execute(DrawingState& a_ds) const;
	string getXMLName() const;
};