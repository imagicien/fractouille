#include "ActionFactory.h"

Action* ActionFactory::createAction(string a_xmlName, vector<double> a_parameters)
{
	Action* action = nullptr;

	if (a_xmlName == "dessineLigne")
	{
		action = (Action*)(new DrawLine());
	}
	else if (a_xmlName == "tourner")
	{
		action = (Action*)(new Tourner());
	}
	else if (a_xmlName == "echelle")
	{
		action = (Action*)(new Echelle());
	}
	else if (a_xmlName == "dessineTriangleEqui")
	{
		action = (Action*)(new DessineTriangleEqui());
	}
	else if (a_xmlName == "changerCouleur")
	{
		action = (Action*)(new ChangerCouleur());
	}
	else if (a_xmlName == "dessineL")
	{
		Action* ligne = createAction("dessineLigne", vector<double>(1, -1));
		Action* tourne = createAction("tourner", vector<double>(1, 90));

		vector<Action*> actions = { ligne, tourne, ligne };
		action = (Action*)(new ActionComposite(actions, "dessineL"));
	}

	action->setParameters(a_parameters);

	return action;
}