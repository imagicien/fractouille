#pragma once

#include <map>
#include <vector>
#include <list>
#include <string>

enum actionDessin{
	DESSINE_LIGNE,
	DESSINE_TRIANGLE_EQUI,
	TOURNER,
	ECHELLE
};
typedef double Couleur[3];
typedef std::pair<actionDessin, std::vector<double>> fonc_param;
typedef std::map<char, fonc_param> instructions_t;
typedef std::map<char, std::list<char>> substitutionRules_t;

using namespace std;