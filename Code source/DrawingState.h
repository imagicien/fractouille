#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include "types.h"
#include "Point.h"
#include "Image.h"


inline double deg2rad(double deg)
{
	double angle = deg * M_PI / 180.0;

	//Ramener sur [0, 2*pi]
	angle = fmod(angle, 2*M_PI);
	if (angle < 0)
		angle += 2*M_PI;

	return angle;
}

struct DrawingState
{
	Image* m_image;
	Point m_position;
	int m_nivRecursion;
	double m_angleCourant;
	Couleur m_couleur;
	double m_longueur;

	Point pointFinal(double angle)
	{
		return Point(m_position.getX() + m_longueur*cos(angleBalise(angle)),
			m_position.getY() + m_longueur*sin(angleBalise(angle)));
	}

	Point pointFinalAngleIncrement(double angle_inc)
	{
		return Point(m_position.getX() + m_longueur*cos(m_angleCourant - deg2rad(angle_inc)),
			m_position.getY() + m_longueur*sin(m_angleCourant - deg2rad(angle_inc)));
	}

	double angleBalise(double angle)
	{
		if (angle + 1 < 1e-5)
			return m_angleCourant;

		else return deg2rad(angle);
	}
};