#pragma once

#include<fstream>
#include<string>
#include<map>
#include<list>
#include<vector>
#include <sstream>

#include "MaitreDessinateur.h"

using namespace std;

class GestionFichier
{
	
public:
	static bool readFichier(string nomFichier, map<char, list<char>> &alphabet, list<char> &initial, instructions_t&, double&);
	static bool writeAlphabet(string nomFichier,map<char, list<char>>);
};