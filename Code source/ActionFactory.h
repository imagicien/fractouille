#pragma once

#include "DrawLine.h"
#include "Tourner.h"
#include "Echelle.h"
#include "DessineTriangleEqui.h"
#include "ChangerCouleur.h"
#include "ActionComposite.h"

class ActionFactory
{
public:
	static Action* createAction(string a_xmlName, vector<double> a_parameters);
};