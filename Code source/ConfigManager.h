#pragma once

#include <fstream>
#include <map>
#include <string>
#include <vector>
#include <iostream>

#include "Fractal.h"
#include "ActionFactory.h"
#include "types.h"

#include "rapidxml.hpp"
#include "rapidxml_print.hpp"

using namespace rapidxml;

class ConfigManager
{
public:
	ConfigManager(string a_configFilePath);
	void read();
	void write();
	Fractal* getFractal(string a_name);
	void addFractal(string a_name, Fractal a_fractal);
private:
	string m_configFilePath;
	vector<char> m_configFileBuffer;
	xml_node<>* m_parentNode;
	map<string, Fractal> m_fractals;
};