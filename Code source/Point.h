#pragma once
#include "CImg.h"
#include <string>
#include <iostream>

using namespace cimg_library;
using namespace std;

class Point {
private:
	double m_x, m_y;

public:
	Point();
	Point(double a_x, double a_y);
	double getX();
	double getY();
	int getIntX();
	int getIntY();
};