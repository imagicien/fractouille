#include "Image.h"


Image::Image() 
{
	this->width = 256;
	this->height = 256;
	image = CImg<double>(width, height, 1, 3, 0);
	
}

Image::Image(string existingFileName)
{
	fileName = existingFileName;
	image = CImg<double>(fileName.c_str());
}

Image::Image(int w, int h)
{
	this->width = w;
	this->height = h;

	image = CImg<double>(width, height, 1, 3, 0);

}

void Image::save(string newFileName)
{
	// file name should contain the extension
	fileName = newFileName;
	image.save(fileName.c_str());
}

int Image::getWidth() const
{
	return width;
}

int Image::getHeight() const
{
	return height;
}

void Image::display()
{
	image.display();
}

void Image::drawLine(Point p1, Point p2, Couleur c)
{
	image.draw_line(p1.getIntX(), p1.getY(), p2.getX(), p2.getY(), c);
}

void Image::drawTriangle(Point p1, Point p2, Point p3, Couleur c)
{
	image.draw_triangle(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY(), c);
}


