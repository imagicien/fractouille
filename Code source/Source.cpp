#define _USE_MATH_DEFINES
#include <iostream>
#include <map>
#include <string>
#include "ChainSubstituer.h"
#include "Image.h"
#include "MaitreDessinateur.h"
#include "ConfigManager.h"
#include "Fractal.h"


using namespace std;

int main()
{
	cout << "                [ FRACTOUILLE ]" << endl;
	cout << "                  --  par  --  " << endl;
	cout << "Alexandre Blouin, Jeremie Coulombe, Marie-Eve Dube," << endl;
	cout << "Marc-Antoine Nadeau, Mathieu Grondin, Carl Lemaire," << endl;
	cout << "      Jean-Philippe Lavoie, Charle Goselin" << endl << endl;
	
	//Charger la configuration
	ConfigManager manager("config.xml");
	manager.read();

	//Choisir une fractale
	Fractal* frac_courante = manager.getFractal("Rose");

	//Initialiser l'image
	Image image(700, 700);

	//Effectuer la substitution
	int niv_recur = 10;
	cout << "Substitution en cours..." << endl;
	cout << "Niveau(x) restant(s)       Progression" << endl;
	ChainSubstituer sub(frac_courante->getSubstitutionRules());
	list<char> sequence = sub.susbtitute(niv_recur, frac_courante->getSeed());

	//Dessiner la fractale
	MaitreDessinateur dessinateur(image, niv_recur);
	dessinateur.setFractal(*frac_courante);
	dessinateur.setPosition(image.getWidth()/2, image.getHeight() - 2);
	dessinateur.modifierLongueur(0.5);
	Couleur JAUNEJAUNEJAUNE = { 255, 100, 100 };
	dessinateur.setCouleur(JAUNEJAUNEJAUNE);
	dessinateur.dessine(sequence);

	//Afficher l'image
	image.display();
	//Enregistrer sur le disque
	image.save("yolo.bmp");

	//Enregistrer la configuration
	manager.write();

	return 0;
}