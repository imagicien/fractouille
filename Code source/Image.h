#pragma once
#include "CImg.h"
#include <string>
#include <iostream>
#include "Point.h"
#include "types.h"

using namespace cimg_library;
using namespace std;

class Image {

	string fileName;
	int width;
	int height;

public : 
	CImg<double> image;
	Image();				// default constructor
	Image(string);			// constructor with the file name
	Image(int,int);			// constructor with image dimensions
	void save(string);		// save an image in a file
	int getWidth() const;	// get image width
	int getHeight() const;	// get image height
	void display();			// displays the image in a window
	void drawLine(Point, Point, Couleur); // draws a line on the CImg
	void drawTriangle(Point, Point, Point, Couleur); // draws a triangle on the CImg

	



};