#pragma once

#include "ActionComposite.h"

ActionComposite::ActionComposite(vector<Action*> a_actions, string a_XMLname)
{
	m_XMLname = a_XMLname;
	m_actions = a_actions;
}
void ActionComposite::execute(DrawingState& a_ds) const
{
	for (Action* action : m_actions)
	{
		action->execute(a_ds);
	}
}

string ActionComposite::getXMLName() const
{
	return m_XMLname;
}