#pragma once

#include "DessineTriangleEqui.h"

void DessineTriangleEqui::execute(DrawingState& a_ds) const
{
	Point p0 = a_ds.m_position;
	Point p1 = a_ds.pointFinal(-1.0);
	Point p2 = a_ds.pointFinalAngleIncrement(60);

	a_ds.m_image->drawTriangle(p0, p1, p2, a_ds.m_couleur);

	a_ds.m_position = p1;
}

string DessineTriangleEqui::getXMLName() const
{
	return "dessineTriangleEqui";
}