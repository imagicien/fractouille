#include "Fractal.h"

list<char> stringToCharList(string imAString);

Fractal::Fractal(string a_name, bool a_isReadOnly = false)
:m_name(a_name),
m_isReadOnly(a_isReadOnly)
{}

Fractal::Fractal()
: m_isReadOnly(0)
{}

Fractal::~Fractal()
{}

void Fractal::setName(string a_name)
{
	m_name = a_name;
}

void Fractal::setSeed(string a_seed)
{
	m_seed = stringToCharList(a_seed);
}

void Fractal::addSubstitutionRule(char a_symbol, string a_substitutionRule)
{
	m_substitutionRules[a_symbol] = stringToCharList(a_substitutionRule);
}

void Fractal::addInstruction(char a_symbol, Action* a_action)
{
	m_actions[a_symbol] = a_action;
}

void Fractal::removeSubstitutionRule(char a_symbol)
{
	substitutionRules_t::iterator i = m_substitutionRules.find(a_symbol);
	m_substitutionRules.erase(i);
}
void Fractal::removeInstruction(char a_symbol)
{
	map<char, Action*>::iterator i = m_actions.find(a_symbol);
	Action* action = i->second;
	delete action;
	m_actions.erase(i);
}

void Fractal::setLengthRatio(double a_lengthRatio)
{
	m_lengthRatio = a_lengthRatio;
}

string Fractal::getName()
{
	return m_name;
}

list<char> Fractal::getSeed()
{
	return m_seed;
}

substitutionRules_t Fractal::getSubstitutionRules()
{
	return m_substitutionRules;
}

map<char, Action*>& Fractal::getActions()
{
	return m_actions;
}

double Fractal::getLengthRatio()
{
	return m_lengthRatio;
}

bool Fractal::isReadOnly()
{
	return m_isReadOnly;
}

list<char> stringToCharList(string imAString)
{
	list<char> listChar;

	for (int i = 0; i < imAString.length(); ++i)
	{
		listChar.push_back(imAString[i]);
	}
	return listChar;
}