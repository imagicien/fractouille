#pragma once

#include "DrawLine.h"

void DrawLine::execute(DrawingState& a_ds) const
{
	Point p = a_ds.pointFinal(m_parameters[0]);
	a_ds.m_image->drawLine(a_ds.m_position, p, a_ds.m_couleur);
	a_ds.m_position = p;
}

string DrawLine::getXMLName() const
{
	return "dessineLigne";
}