#pragma once
#define _USE_MATH_DEFINES

#include <cmath>
#include <stdio.h>

#include "types.h"
#include "Point.h"
#include "Image.h"
#include "Fractal.h"
#include "DrawingState.h"

class MaitreDessinateur
{
public:
	MaitreDessinateur(Image& a_image, int a_nivRecurtion);
	~MaitreDessinateur();

	void dessine(list<char> a_sequence);
	void setFractal(Fractal a_fractal);
	void setPosition(int x, int y);
	void modifierLongueur(double);
	void setCouleur(Couleur a_couleur);

private:
	DrawingState m_drawingState;
	Fractal m_fractal;
};