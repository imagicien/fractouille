#include "ConfigManager.h"

Fractal readFractal(xml_node<>* fractalPresetNode, bool a_isReadOnly = false);
string charListToString(list<char> a_list);

ConfigManager::ConfigManager(string a_configFilePath)
:m_configFileBuffer((istreambuf_iterator<char>(ifstream(a_configFilePath))), istreambuf_iterator<char>()),
m_configFilePath(a_configFilePath)
{
	m_configFileBuffer.push_back('\0');

	xml_document<> doc;
	doc.parse<0>(&m_configFileBuffer[0]);
	m_parentNode = doc.first_node("fractouilleConfig");

	//Est-ce que le fichier existe?
	if (m_parentNode == nullptr)
	{
		cout << "Le fichier XML n'existe pas!" << endl;
		int fat_bobby;
		cin >> fat_bobby;
	}
}

void ConfigManager::addFractal(string a_name, Fractal a_fractal)
{
	m_fractals[a_name] = a_fractal;
}

Fractal* ConfigManager::getFractal(string a_name)
{
	return &m_fractals[a_name];
}

void ConfigManager::read()
{
	//Fractales lecture seule
	xml_node<>* readOnlyFracNode = m_parentNode->first_node("readOnly");
	for (xml_node<>* presetItem = readOnlyFracNode->first_node("fractal"); presetItem != nullptr; presetItem = presetItem->next_sibling())
	{
		Fractal preset = readFractal(presetItem, true);
		m_fractals[preset.getName()] = preset;
	}

	//Fractales custom
	xml_node<>* customFracNode = m_parentNode->first_node("custom");
	for (xml_node<>* presetItem = customFracNode->first_node("fractal"); presetItem != nullptr; presetItem = presetItem->next_sibling())
	{
		Fractal preset = readFractal(presetItem);
		if (m_fractals.find(preset.getName()) != m_fractals.end()) //le nom existe deja!
		{
			preset.setName("NOM ERRON�");
		}
		m_fractals[preset.getName()] = preset;
	}
}

Fractal readFractal(xml_node<>* fractalPresetNode, bool a_isReadOnly)
{
	Fractal preset(fractalPresetNode->first_attribute("name")->value(), a_isReadOnly);
	xml_node<>* childNode;

	// SUBSTITUTION
	childNode = fractalPresetNode->first_node("substitution");

	for (xml_node<>* substItem = childNode->first_node("rule"); substItem != nullptr; substItem = substItem->next_sibling())
	{
		xml_attribute<>* attribute = substItem->first_attribute();
		string symbol = attribute->value();
		attribute = attribute->next_attribute();
		string substitution = attribute->value();
		preset.addSubstitutionRule(symbol[0], substitution);
	}

	// SEED
	preset.setSeed(fractalPresetNode->first_node("seed")->first_attribute()->value());

	// ACTION
	childNode = fractalPresetNode->first_node("actionList");

	for (xml_node<>* actionItem = childNode->first_node("action"); actionItem != nullptr; actionItem = actionItem->next_sibling())
	{
		xml_attribute<>* attribute = actionItem->first_attribute();
		char symbol = (attribute->value())[0];
		attribute = attribute->next_attribute();
		string function = attribute->value();
		vector<double> params;

		for (xml_node<>* paramItem = actionItem->first_node("param"); paramItem != nullptr; paramItem = paramItem->next_sibling())
		{
			xml_attribute<>* attribute = paramItem->first_attribute();
			string value = attribute->value();
			params.push_back(std::stod(value));
		}
		Action* act = ActionFactory::createAction(function, params);
		preset.addInstruction(symbol, act);
	}

	// LENGTH RATIO
	preset.setLengthRatio(stod(fractalPresetNode->first_node("lengthRatio")->first_attribute()->value()));

	return preset;
}

void ConfigManager::write()
{
	xml_document<> doc;

	//Creer noeud fractouilleConfig
	xml_node<> *fractouilleConfigNode = doc.allocate_node(node_element, "fractouilleConfig");
	doc.append_node(fractouilleConfigNode);

	//Creer noeud readOnly
	xml_node<> *readOnlyNode = doc.allocate_node(node_element, "readOnly");
	fractouilleConfigNode->append_node(readOnlyNode);

	//Creer noeud custom
	xml_node<> *customNode = doc.allocate_node(node_element, "custom");
	fractouilleConfigNode->append_node(customNode);

	//�CRIRE FRACTALES =================================================================
	for (auto fracPair : m_fractals)
	{
		Fractal frac = fracPair.second;

		//Creer noeud fractal
		xml_node<> *fractalNode = doc.allocate_node(node_element, "fractal");
		if (frac.isReadOnly())
		{
			readOnlyNode->append_node(fractalNode);
		}
		else
		{
			customNode->append_node(fractalNode);
		}

		//Creer attribut name
		char* nameStr = doc.allocate_string(frac.getName().c_str()); //allouer char array
		xml_attribute<> *nameAttr = doc.allocate_attribute("name", nameStr);
		fractalNode->append_attribute(nameAttr);

		//Creer noeud substitution -------------------------------------------------
		xml_node<> *substNode = doc.allocate_node(node_element, "substitution");
		fractalNode->append_node(substNode);

		//Remplir substitution
		for (auto rulePair : frac.getSubstitutionRules())
		{
			//Creer noeud rule
			xml_node<> *ruleNode = doc.allocate_node(node_element, "rule");
			substNode->append_node(ruleNode);
			//Creer attribut symbol
			char symbolCStr[2] = { rulePair.first, '\0'}; //une C string est un tableau de char termin� par le "caract�re nul"
			char* symbolStr = doc.allocate_string(symbolCStr); //allouer char array
			xml_attribute<> *symbolAttr = doc.allocate_attribute("symbol", symbolStr);
			ruleNode->append_attribute(symbolAttr);
			//Creer attribut substitution
			char* subsStr = doc.allocate_string(charListToString(rulePair.second).c_str()); //allouer char array
			xml_attribute<> *subsAttr = doc.allocate_attribute("substitution", subsStr);
			ruleNode->append_attribute(subsAttr);
		}

		//Creer noeud seed ----------------------------------------------------------
		xml_node<> *seedNode = doc.allocate_node(node_element, "seed");
		fractalNode->append_node(seedNode);
		
		char* seedStr = doc.allocate_string(charListToString(frac.getSeed()).c_str());
		xml_attribute<> *stringAttr = doc.allocate_attribute("string", seedStr);
		seedNode->append_attribute(stringAttr);

		//Creer noeud ActionList-----------------------------------------------------
		xml_node<> *actionListNode = doc.allocate_node(node_element, "actionList");
		fractalNode->append_node(actionListNode);

		//Remplir ActionList
		for (pair<char, Action*> actionPair : frac.getActions())
		{
			//Creer noeud action
			xml_node<> *actionNode = doc.allocate_node(node_element, "action");
			actionListNode->append_node(actionNode);

			//Creer attribut symbol
			char symbolCStr[2] = { actionPair.first, '\0' }; //une C string est un tableau de char termin� par le "caract�re nul"
			char* symbolStr = doc.allocate_string(symbolCStr); //allouer char array
			xml_attribute<> *symbolAttr = doc.allocate_attribute("symbol", symbolStr);
			actionNode->append_attribute(symbolAttr);

			//Creer attribut func
			Action* action = actionPair.second;
			char* funcStr = doc.allocate_string(action->getXMLName().c_str()); //allouer char array
			xml_attribute<> *funcAttr = doc.allocate_attribute("func", funcStr);
			actionNode->append_attribute(funcAttr);

			for (double param : action->getParameters())
			{
				//Creer noeud action
				xml_node<> *paramNode = doc.allocate_node(node_element, "param");
				actionNode->append_node(paramNode);

				//Creer noeud param de action
				char* paramStr = doc.allocate_string(std::to_string(param).c_str()); //allouer char array
				xml_attribute<> *paramAttr = doc.allocate_attribute("value", paramStr);
				paramNode->append_attribute(paramAttr);
			}
		}

		//Creer noeud lengthRatio ----------------------------------------------------------
		xml_node<> *lengthRatioNode = doc.allocate_node(node_element, "lengthRatio");
		fractalNode->append_node(lengthRatioNode);

		char* lengthRatioStr = doc.allocate_string(std::to_string(frac.getLengthRatio()).c_str());
		xml_attribute<> *lengthRatioAttr = doc.allocate_attribute("value", lengthRatioStr);
		lengthRatioNode->append_attribute(lengthRatioAttr);
	}

	ofstream fichier(m_configFilePath);
	fichier << doc;
	fichier.close();
}

string charListToString(list<char> a_list)
{
	string retour;
	for (char car : a_list)
		retour.push_back(car);
	return retour;
}