#include"GestionFichier.h"

bool GestionFichier::readFichier(string nomFichier, map<char, list<char>> &alphabet, list<char> &initial, instructions_t &instructions, double &longueur)
{
	ifstream fichier;
	string ligne;
	list<char> substitution;

	alphabet.clear();
	initial.clear();
	fichier.open(nomFichier, ios::in);
		
	if (fichier.is_open())
	{
		getline(fichier, ligne);
		while (ligne[0]!= '-')
		{
		    //Lecture de l'alphabet      
			string str2 = ligne.substr(2, ligne.size());
			substitution.clear();
			for (size_t i = 0; i < str2.size(); i++)
			{
				substitution.push_back(str2[i]);
			}
			alphabet.insert(pair<char, list<char>>(ligne[0], substitution));
			getline(fichier, ligne);
		}
		//Lecture Initial
		getline(fichier, ligne);
		for (int i = 0; i < ligne.size(); i++)
		{
			initial.push_back(ligne[i]);
		}
		getline(fichier,ligne);

		//Lecture fonctions
		char element;
		string act;
		vector<double> param;
		map<string, actionDessin> correspondanceAction = { { "dessineLigne", DESSINE_LIGNE }, { "dessineTriangleEqui", DESSINE_TRIANGLE_EQUI }, { "tourner", TOURNER }, { "echelle", ECHELLE } };
		actionDessin action;
		getline(fichier, ligne);
		double temp;

		while (ligne[0] != '-')
		{
			element = ligne[0];
			act = ligne.substr(2, ligne.find(",")-2);
			action = correspondanceAction[act];
			ligne = ligne.substr(ligne.find(",") + 1, ligne.size());
			param.clear();
			while (ligne != "")
			{
				string num = ligne.substr(0, ligne.find(","));
				istringstream buffer(num);
				buffer >> temp;
				param.push_back(temp);
				ligne = ligne.substr(num.size()+1, ligne.size());
			}

			instructions[element] = make_pair(action, param);
			

			getline(fichier, ligne);
		}

		// longueur
		double tmp;
		getline(fichier, ligne);
		string num = ligne.substr(0, ligne.find(","));
		istringstream buffer(num);
		buffer >> tmp;
		longueur = tmp;

		fichier.close();
		return true;
	}

	return false;
}

bool GestionFichier::writeAlphabet(string nomFichier, map<char, list<char>> alphabet)
{
	ofstream fichier;
	fichier.open(nomFichier);
	
	 //pour parcourir toutes les paires de la map
	if (fichier.is_open())
	{
		for (map<char, list<char>>::iterator it = alphabet.begin(); it != alphabet.end(); ++it)
		{
			fichier << it->first;
			fichier << ":";

			for (std::list<char>::const_iterator iterator = (it->second).begin(); iterator != it->second.end(); ++iterator)
			{
				fichier << *iterator;
			}

			fichier << "\n";
			
		}
		return true;
	}
	fichier.close();
	return false;
}

