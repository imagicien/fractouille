#include "Action.h"


void Action::execute(DrawingState& a_ds) const
{}

vector<double> Action::getParameters() const
{
	return m_parameters;
}

void Action::setParameters(vector<double> params)
{
	m_parameters = params;
}