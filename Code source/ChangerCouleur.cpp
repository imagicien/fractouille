#pragma once

#include "ChangerCouleur.h"

void ChangerCouleur::execute(DrawingState& a_ds) const
{
	a_ds.m_couleur[0] = fmod(a_ds.m_couleur[0] + m_parameters[0], 255.0);
	a_ds.m_couleur[1] = fmod(a_ds.m_couleur[1] + m_parameters[1], 255.0);
	a_ds.m_couleur[2] = fmod(a_ds.m_couleur[2] + m_parameters[2], 255.0);
}

string ChangerCouleur::getXMLName() const
{
	return "changerCouleur";
}