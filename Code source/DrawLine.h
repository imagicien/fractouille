#pragma once

#include "Action.h"



class DrawLine : public Action
{
public :
	void execute(DrawingState& a_ds) const;
	string getXMLName() const;
};