#include "ChainSubstituer.h"

#include <iomanip>

ChainSubstituer::ChainSubstituer(void)
{}

ChainSubstituer::ChainSubstituer(map<char,list<char>> rules):
m_rules(rules)
{}

ChainSubstituer::~ChainSubstituer(void)
{}

list<char> ChainSubstituer::susbtitute(int x, list<char> chain)
{
	if(x > 0)
	{
		int j = 1;
		int total = chain.size();
		for (list<char>::iterator i = chain.begin(); i != chain.end(); )
		{
			char aremplacer = *i;
			i = chain.erase(i);
			list<char> remplacement = list<char>(m_rules[aremplacer]);
			chain.splice(i, remplacement);

			cout << '\r' << "         " << setw(2) << x << "                    ";
			cout << setw(3) << 100 * j / total << "%" << flush;
			j++;
		}
		chain = susbtitute(--x, chain);
	}

	return chain;
}