#pragma once

#include "Action.h"



class ActionComposite : public Action
{
public :
	ActionComposite(vector<Action*> a_actions,string a_XMLname);
	void execute(DrawingState& a_ds) const;
	string getXMLName() const;
private:
	string m_XMLname;
	vector<Action*> m_actions;
};