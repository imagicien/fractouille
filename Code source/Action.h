#pragma once



#include <string>
#include <vector>

#include "DrawingState.h"

using namespace std;

class Action
{
public:
	virtual void execute(DrawingState& a_ds) const = 0;

	virtual string getXMLName() const = 0;
	vector<double> getParameters() const;
	void setParameters(vector<double>);

protected:
	vector<double> m_parameters;
};